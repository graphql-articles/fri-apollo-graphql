import express from 'express'

import { User } from '../models'
import log from '../log'

const router = new express.Router()

/**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Get user.
 *     description: Get user by ID.
 *     tags:
 *       - User
 *     parameters:
 *       - name: id
 *         description: ID of user
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/User'
 */
router.get( '/:id', async ( req, res, next ) => {
    log.info( `Fetching user by id ... `, req.params, req.headers )

	const id = req.params.id

	try {
		let user = await User.findById( id )
		return res.send( { user } )
	} catch ( e ) {
		log.error( `An error occured: `, e )
		return next( {
			msg: e
		} )
	}
} )

/**
 * @swagger
 * /users:
 *   post:
 *     summary: Create a new user.
 *     description: Create a new blog user.
 *     tags:
 *       - User
 *     parameters:
 *       - name: Parameters for creating a user.
 *         description: Must input email and name.
 *         in: body
 *         required: true
 *         schema:
 *          properties:
 *           email:
 *            type: string
 *           name:
 *            type: string
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/User'
 */
router.post( '/', async ( req, res, next ) => {
    log.info( `Creating new user ... `, req.body, req.headers )

    const { name, email } = req.body

	try {
		let user = new User( {
			name,
			email
		} )
		await user.save()

		return res.send( { user } )
	} catch ( e ) {
		log.error( `An error occured: `, e )
		return next( {
			msg: e
		} )
	}
} )

export default router