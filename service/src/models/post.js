import mongoose from 'mongoose'

const Schema = mongoose.Schema

const PostSchema = new Schema({
    content: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
})

PostSchema.options.toJSON = PostSchema.options.toJSON || {}
PostSchema.options.toJSON.transform = ( doc, ret ) => {
	ret.id = ret._id
	return ret
}

const Post = mongoose.model( 'post', PostSchema )

/**
 * @swagger
 * definitions:
 *   Post:
 *     type: object
 *     properties:
 *       _id:
 *         type: string
 *         default: objectId
 *       content:
 *         description: Content of post.
 *         type: string
 *         required: true
 *       user:
 *         description: ID of user that is inserting post.
 *         type: string
 *         default: objectId
 */

export default Post