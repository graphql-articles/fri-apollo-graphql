import express from 'express'

import { Comment } from '../models'
import log from '../log'

const router = new express.Router()

/**
 * @swagger
 * /comments/posts/{id}:
 *   get:
 *     summary: Get comments by post ID.
 *     description: Get comments by post ID.
 *     tags:
 *       - Comment
 *     parameters:
 *       - name: id
 *         description: ID of post
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Comments of posts
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Comment'
 */
router.get( '/posts/:id', async ( req, res, next ) => {
    log.info( `Fetching comments by post id ... `, req.body, req.headers )

	const id = req.params.id

	try {
		let comment = await Comment.find( { post: id } )
		log.info( `FOOBAR,,,,`, comment )
		return res.send( { comment } )
	} catch ( e ) {
		log.error( `An error occured: `, e )
		return next( {
			msg: e
		} )
	}
} )

/**
 * @swagger
 * /comments/{id}:
 *   get:
 *     summary: Get comment.
 *     description: Get comment by ID.
 *     tags:
 *       - Comment
 *     parameters:
 *       - name: id
 *         description: ID of comment
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/Comment'
 */
router.get( '/:id', async ( req, res, next ) => {
    log.info( `Fetching comment by id ... `, req.body, req.headers )

	const id = req.params.id

	try {
		let comment = await Comment.findById( id )
		return res.send( { comment } )
	} catch ( e ) {
		log.error( `An error occured: `, e )
		return next( {
			msg: e
		} )
	}
} )

/**
 * @swagger
 * /comments:
 *   post:
 *     summary: Create a new Comment.
 *     description: Create a new comment belonging to user and post.
 *     tags:
 *       - Comment
 *     parameters:
 *       - name: Parameters for creating a new comment.
 *         description: Must input comment, user id and post id.
 *         in: body
 *         required: true
 *         schema:
 *          properties:
 *           comment:
 *            type: string
 *            required: true
 *           user:
 *            type: string
 *            required: true
 *           post:
 *            type: string
 *            required: true
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/Comment'
 */
router.post( '/', async ( req, res, next ) => {
	log.info( `Creating new comment ... `, req.body, req.headers )
	
	const { user, post, comment } = req.body

	try {
		let commentObj = new Comment( {
			user,
			post,
			comment
		} )
		await commentObj.save()

		return res.send( { comment: commentObj } )
	} catch ( e ) {
		log.error( `An error occured: `, e )
		return next( {
			msg: e
		} )
	}
} )

export default router