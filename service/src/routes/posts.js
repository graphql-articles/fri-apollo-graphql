import express from 'express'

import { Post } from '../models'
import log from '../log'

const router = new express.Router()

/**
 * @swagger
 * /posts/{id}:
 *   get:
 *     summary: Get post.
 *     description: Get post by ID.
 *     tags:
 *       - Post
 *     parameters:
 *       - name: id
 *         description: ID of post
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/Post'
 */
router.get( '/:id', async ( req, res, next ) => {
    log.info( `Fetching post by id ... `, req.body, req.headers )

	const id = req.params.id

	try {
		let post = await Post.findById( id )
		return res.send( { post } )
	} catch ( e ) {
		log.error( `An error occured: `, e )
		return next( {
			msg: e
		} )
	}
} )

/**
 * @swagger
 * /posts:
 *   post:
 *     summary: Create a new Post.
 *     description: Create a new post belonging to user.
 *     tags:
 *       - Post
 *     parameters:
 *       - name: Parameters for creating a new blog post.
 *         description: Must input content and user id.
 *         in: body
 *         required: true
 *         schema:
 *          properties:
 *           user:
 *            type: string
 *            required: true
 *           content:
 *            type: string
 *            required: true
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/Post'
 */
router.post( '/', async ( req, res, next ) => {
    log.info( `Creating new post ... `, req.body, req.headers )

    const { user, content } = req.body

	try {
		let post = new Post( {
			user,
			content
		} )
		await post.save()

		return res.send( { post } )
	} catch ( e ) {
		log.error( `An error occured: `, e )
		return next( {
			msg: e
		} )
	}
} )

export default router