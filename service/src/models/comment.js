import mongoose from 'mongoose'

const Schema = mongoose.Schema

const CommentSchema = new Schema({
    comment: {
        type: String
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    post: {
        type: Schema.Types.ObjectId,
        ref: 'Post'
    }
})

CommentSchema.options.toJSON = CommentSchema.options.toJSON || {}
CommentSchema.options.toJSON.transform = ( doc, ret ) => {
	ret.id = ret._id
	return ret
}

const Comment = mongoose.model( 'comment', CommentSchema )

/**
 * @swagger
 * definitions:
 *   Comment:
 *     type: object
 *     properties:
 *       _id:
 *         type: string
 *         default: objectId
 *       comment:
 *         description: Content of comment.
 *         type: string
 *         required: true
 *       user:
 *         description: ID of user that is inserting post.
 *         type: string
 *         default: objectId
 *       post:
 *         description: ID of post that we are commenting to.
 *         type: string
 *         default: objectId
 */

export default Comment