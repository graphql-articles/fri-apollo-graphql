import { mergeTypes } from 'merge-graphql-schemas'

import * as types from './types.gql'

const userSchema = mergeTypes( [
	types,
] )

export default userSchema