import express from 'express'

import users from './users'
import posts from './posts'
import comments from './comments'

const router = new express.Router()

router.get( '/', async ( req, res ) => {
	res.send( { msg: 'Invalid request.' } )
} )

router.use( '/users', users )
router.use( '/posts', posts )
router.use( '/comments', comments )

export default router
