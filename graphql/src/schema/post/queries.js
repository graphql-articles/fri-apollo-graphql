export default {
    post: async( _source, { input: { id } }, { dataSources } ) => {
        return dataSources.blogAPI.getPost( id )
    },
}