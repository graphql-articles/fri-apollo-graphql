import path from 'path'
import swagger from 'swagger-jsdoc'

const swaggerDefinition = {
	info: {
		title: 'FRI GraphQL Blog Service',
		version: '1.0.0',
		description: 'FRI GraphQL Blog Service.',
	},
}

const options = {
	swaggerDefinition,
	apis: [
		path.resolve( 'src/routes/**/*.js' ),
		path.resolve( 'src/models/**/*.js' ),
	],
}

const swaggerSpec = swagger( options )

export default swaggerSpec
