### Running project

Docker is required to be installed.

To run the project:

```bash
docker network create fri-graphql
docker-compose up
```

Note: If containers need to be rebuilt, before running compose run:

```bash
docker-compose build
```

The applications run on the following URLs:

- GraphQL: localhost:81/graphql
- Blog service: localhost:82
- Blog service documentation (Swagger): localhost:82/docs