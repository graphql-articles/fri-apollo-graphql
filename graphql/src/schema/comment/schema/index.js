import { mergeTypes } from 'merge-graphql-schemas'

import * as types from './types.gql'

const commentSchema = mergeTypes( [
	types,
] )

export default commentSchema