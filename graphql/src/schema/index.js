import { mergeTypes, mergeResolvers } from 'merge-graphql-schemas'

import {
	schema as userSchema,
	resolvers as userResolvers,
} from './user'

import {
	schema as postSchema,
	resolvers as postResolvers,
} from './post'

import {
	schema as commentSchema,
	resolvers as commentResolvers,
} from './comment'

const typeDefs = mergeTypes( [
	userSchema,
	postSchema,
	commentSchema
] )

const resolvers = mergeResolvers( [
	userResolvers,
	postResolvers,
	commentResolvers,
] )

export { typeDefs, resolvers }