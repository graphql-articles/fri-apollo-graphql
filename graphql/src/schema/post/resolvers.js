import queries from "./queries"

export default {
    Query: queries,
    Post: {
        user: async( _source, _args, { dataSources } ) => {
            return dataSources.blogAPI.getUser( _source.user )
        },
        comments: async( _source, _args, { dataSources } ) => {
            return dataSources.blogAPI.getPostComments( _source.id )
        },
    }
}