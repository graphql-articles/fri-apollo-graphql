import { RESTDataSource } from "apollo-datasource-rest"

let blogEntity = null

class BlogEntity extends RESTDataSource {
	constructor() {
		if ( ! blogEntity ) {
			super()
			this.baseURL = "http://fri_graphql_blog_service:8080/"
			blogEntity = this
		}
		return blogEntity
	}

	async getPost( id ) {
		const data = await this.get( `posts/${ id }` )
		return data.post
	}

	async getUser( id ) {
		const data = await this.get( `users/${ id }` )
		return data.user
	}

	async getPostComments( id ) {
		const data = await this.get( `comments/posts/${ id }` )
		return data.comment
	}

}

new BlogEntity()

export default blogEntity
